require('dotenv').config();

module.exports = {
  port: process.env.PORT || 3000,
  path: {
    unprotected: /^\/(check)|(auth)|(operations)|(users)|(twitters)/,
    protected: /^\/(config)/
  },
  jwtRules: {
    secretFrase: process.env.SECRET_FRASE || 'secret',
    expTime: process.env.EXP_TIME || '1m'
  }
};
