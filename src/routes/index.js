const checkRouter = require('./check.router');
const authRouter = require('./auth.router');
const operationsRouter = require('./operations.router');
const configRouter = require('./config.router');

module.exports = {
  checkRouter,
  authRouter,
  operationsRouter,
  configRouter
};