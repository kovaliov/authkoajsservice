const router = require('koa-joi-router');
const Joi = router.Joi;
const operations = router();
const guid = require('guid');

operations.route({
  method: 'get',
  path: '/operations',
  handler: async (ctx, next) => {
    // log.info(store.operations[0].request);

    ctx.body = {
      operations: store.operations
    };
  }
});

operations.route({
  method: 'get',
  path: '/operations/:id',
  validate: {
    output: {
      200: {
        body: {
          operation: Joi.object()
        }
      }
    }
  },
  handler: async (ctx, next) => {
    const { id } = ctx.request.params;
    const isOperationHas = store.operations.some(operation => operation._id === id);

    if (!isOperationHas) {
      ctx.throw(404, {
        operation: {}
      });
    }

    const operation = store.operations.filter(operation => operation._id === id);

    ctx.body = {
      operation: operation[0]
    };
  }
});

module.exports = operations;
