const router = require('koa-joi-router');
const Joi = router.Joi;
const config = router();


config.route({
  method: 'get',
  path: '/config',
  validate: {
    200: {
      body: {
        port: Joi.number().required()
      }
    }
  },
  handler: async (ctx) => {
    const config = require('../../config/config');

    ctx.body = {
      port: config.port
    }
  }
});

module.exports = config;