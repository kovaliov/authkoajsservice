const Router = require('koa-router');
const guid = require('guid');
const jwt = require('jsonwebtoken');
const bodyParser = require('koa-bodyparser');

const twitters = new Router({
  prefix: '/twitters'
});

twitters.use(bodyParser());

twitters
  .get('/', async (ctx) => {
    const _operation_id = guid.raw();

    ctx.body = {
      status: 200,
      statusCode: 200,
      twitters: store.twitters,
      _operation_id
    };

    setOperation(ctx);
  })
  .post('/', async (ctx) => {
    const { text, userId } = ctx.request.body;
    const _id = guid.raw();
    const _operation_id = guid.raw();
    const twitter = Object.assign({}, {
      _id,
      text,
      userId
    });

    ctx.body = {
      status: 200,
      statusCode: 200,
      twitter,
      _operation_id
    }

    store.addTwitter(twitter);

    setOperation(ctx);
  })
  .put('/', async (ctx) => {
    const { _id }  = ctx.request.body;
    const _operation_id = guid.raw();

    store.twitters = store.twitters.map((twitter) => {
      if (twitter._id == _id) {
        return ctx.request.body;
      }

      return twitter;
    });

    ctx.body = {
      status: 200,
      statusCode: 200,
      twitter: ctx.request.body,
      _operation_id
    };

    setOperation(ctx);
  })
  .del('/:id', async (ctx) => {
    const { id } = ctx.params;
    const { _operation_id } = guid.raw();

    store.twitters = store.twitters.filter(twitter => twitter._id !== id);

    ctx.body = {
      status: 200,
      statusCode: 200,
      _operation_id
    };

    setOperation(ctx);
  });

module.exports = twitters;
