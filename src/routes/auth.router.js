const router = require('koa-joi-router');
const Joi = router.Joi;
const auth = router();
const guid = require('guid');
const jwt = require('jsonwebtoken');
const {
  secretFrase: SECRET_FRASE,
  expTime: EXP_TIME } = require('../../config/config').jwtRules;

auth.route({
  method: 'post',
  path: '/auth/signup',
  validate: {
    body: {
      email: Joi.string().max(60).required(),
      password: Joi.string().max(20).required()
    },
    type: 'json',
    output: {
      200: {
        body: {
          _id: Joi.string().required(),
          email: Joi.string(),
          _operation_id: Joi.string(),
          createdDate: Joi.string(),
          isAdmin: Joi.boolean()
        }
      }
    }
  },
  handler: async (ctx) => {
    const _operation_id = guid.raw();
    const _id = guid.raw();
    const { email, password } = ctx.request.body;
    const createdDate = JSON.stringify(new Date());

    store.users.push({
      _id,
      email,
      password,
      createdDate,
      isAdmin: false
    });

    ctx.status = 200;
    ctx.body = {
      _id,
      email,
      _operation_id,
      createdDate,
      isAdmin: false
    };
    console.log(store.users);
    setOperation(ctx);
  }
});

auth.route({
  method: 'post',
  path: '/auth/signin',
  validate: {
    body: {
      email: Joi.string().max(60).required(),
      password: Joi.string().max(20).required(),
      isRemember: Joi.string() || false
    },
    type: 'json',
    output: {
      200: {
        body: {
          user: Joi.object(),
          token: Joi.string()
        }
      }
    }
  },
  handler: async (ctx) => {
    const _operation_id = guid.raw();
    const _id = guid.raw();
    const { email, password } = ctx.request.body;

    const user = store.users.find(u => u.email === email);
    console.log(user);

    const token = jwt.sign({
      data: {
        user
      }
    }, SECRET_FRASE, { expiresIn: '30s' });
    // const userData = { _id, _operation_id, email, password, token };

    ctx.set('authorization', `Bearer ${token}`);
    ctx.body = {
      user,
      token
    };

    setOperation(ctx);
  }
});

auth.route({
  method: 'put',
  path: '/auth/isAlive',
  handler: async (ctx) => {
    const _operation_id = guid.raw();
    const token = ctx.request.header['authorization'].split(' ')[1];

    ctx.body = {
      _operation_id
    };

    setOperation(ctx);
  }
});

module.exports = auth;
