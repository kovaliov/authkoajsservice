const Router = require('koa-router');
const guid = require('guid');
const jwt = require('jsonwebtoken');
const bodyParser = require('koa-bodyparser');

const users = new Router({
  prefix: '/users'
});

users.use(bodyParser());

users
  .get('/:id', async (ctx) => {
    const { id } = ctx.params;

    ctx.body = {
      id
    };
  })
  .get('/:id/twitters', async (ctx) => {
    const { id } = ctx.params;
    const start = ctx.request.query['start'] || 10;
    const end = ctx.request.query['end'] || 0;

    const notes = store.twitters.filter(twitter => twitter.userId === id);
    const user = store.users.filter(user => user._id === id);
    console.log(user);

    ctx.body = {
      id,
      start,
      end,
      user: user[0],
      notes
    };
  })
  .post('/', async (ctx) => {

  })
  .put('/', async (ctx) => {


  })
  .del('/:id', async (ctx) => {

  });

module.exports = users;
