const router = require('koa-joi-router');
const Joi = router.Joi;
const check = router();

check.route({
  method: 'get',
  path: '/check',
  handler: async (ctx) => {
    const isCheck = true;

    ctx.body = {
      isCheck
    };
  }
});

module.exports = check;