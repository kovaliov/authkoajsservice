// X Response Time Middlewares

module.exports = async function(ctx, next) {
  const start = new Date();

  await next();
  
  const ms = new Date() - start;
  
  ctx.set('X-Response-Time', `${ms}ms`);
};