const xResponseTimeMiddleware = require('./x-response-time.middleware');
const loggerMiddleware = require('koa-logger')();
const corsMiddleware = require('kcors')();
const operationsMiddleware = require('./operations.middleware');

module.exports = {
  xResponseTimeMiddleware,
  loggerMiddleware, 
  corsMiddleware,
  operationsMiddleware
};