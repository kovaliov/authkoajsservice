const Koa = require('koa');
const app = new Koa();
const jwt = require('koa-jwt');
const bodyParser = require('koa-bodyparser');
const config = require('../config/config');
const {
  xResponseTimeMiddleware,
  loggerMiddleware,
  corsMiddleware,
  operationsMiddleware } = require('./middlewares');
 /*
  * Define routes by Joi-router lib.
  */
const routers = require('./routes');

 /*
  * Define routes by koa-router lib.
  */
const twittersRouter = require('./routes/twitter.router');
const usersRouter = require('./routes/users.router');

const guid = require('guid');

global.store = {
  users: [
    {
      _id: "88dec53c-8421-d666-d973-ac76db47fca8",
      email: 'vlad1',
      password: 'vlad1'
    },
    {
      _id: guid.raw(),
      email: 'vlad2',
      password: 'vlad2'
    }
  ],
  operations: [],
  addUser: (user) => {
    this.users.push(user);
  },
  twitters: [],
  addTwitter: (twitter) => {
    store.twitters.push(twitter);
  }
};
global.log = require('winston');
global.setOperation = function (ctx) {
  const operationCtx = Object.assign({}, {
    request: {
      request: ctx.request,
      body: ctx.request.body
    },
    response: {
      response: ctx.response,
      body: ctx.response.body
    }
  }, {
    _id: ctx.response.body._operation_id
  });

  store.operations.push(operationCtx);
};

app.use(jwt({ secret: config.secretFrase }).unless({ path: [config.path.unprotected] }));
app.use(corsMiddleware);
app.use(xResponseTimeMiddleware);
app.use(loggerMiddleware);

app.use(usersRouter.routes());
app.use(twittersRouter.routes());


for (let routerName in routers) {
  app.use(routers[routerName].middleware());
}



app.use(async (ctx, next) => {
  console.log(ctx.params);
});

app.listen(config.port, (data) => {
  console.log('Restarted!!!');
});
